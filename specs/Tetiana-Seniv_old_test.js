const assert = require('assert');
const { URL } = require('url');

function userLogin(email, password) {
  const emailField = $('input[name="email"]');
  const passField = $('input[type="password"]');
  const loginButton = $('button.is-primary');

  emailField.clearValue();
  emailField.setValue(email);
  passField.clearValue();
  passField.setValue(password);
  loginButton.click();
}

function waitForMap() {
  const map = $('div#map');
  map.waitForDisplayed(5000);
}

describe('Hedonist', () => {
  const random = Math.floor(Math.random() * 1000000);
  const testMail = `test-mail-${random}@gmail.com`;
  const newListTitle = `Baba-${random}`;

  xit('should register on Hedonist', () => {
    browser.url('https://staging.bsa-hedonist.online/signup');
    
    const firstnameField = $('input[name=firstName]');
    const lastnameField = $('input[name=lastName]');
    const emailField = $('input[name=email]');
    const newpassField = $('input[type=password]');
    const createButton = $('button.button.is-primary');
    const notification = $('.notices.is-top');

    firstnameField.setValue('QWE');
    lastnameField.setValue('RTY');
    emailField.setValue(testMail);
    newpassField.setValue('123456789');
    createButton.click();
    notification. waitForDisplayed();

    assert.strictEqual(notification.getText(), "You have successfully registered! Now you need to login");

    const actualUrl = browser.getUrl();
    
    assert.equal(actualUrl, "https://staging.bsa-hedonist.online/login");

    //register with created user
    browser.url('https://staging.bsa-hedonist.online/signup');

    const usedfirstnameField = $('input[name=firstName]');
    const usedlastnameField = $('input[name=lastName]');
    const usedemailField = $('input[name=email]');
    const usednewpassField = $('input[type=password]');
    const usedcreateButton = $('button.button.is-primary');
    const invalidEmailField = $('div.error');

    usedfirstnameField.setValue('QWE');
    usedlastnameField.setValue('RTY');
    usedemailField.setValue('12QWE@yandex.ru');
    usednewpassField.setValue('123456789');
    usedcreateButton.click();
    
    assert.strictEqual(invalidEmailField.getText(), "This email is already registered.");
    
    const usedactualUrl = browser.getUrl();
    
    assert.equal(usedactualUrl, "https://staging.bsa-hedonist.online/signup");

    browser.reloadSession();
  });

  xit('should login to Hedonist', () => {
    browser.url('https://staging.bsa-hedonist.online');
    
    const emailField = $('input[name=email]');
    const passField = $('input[type=password]');
    const loginButton = $('button.button.is-primary');
    const notification = $('.notices.is-top');

    emailField.setValue('12QWE@yandex.ru');
    passField.setValue('123456789');
    loginButton.click();
    notification.waitForDisplayed();

    assert.strictEqual(notification.getText(), "Welcome!");

    waitForMap();

    const url =  new URL(browser.getUrl());
    const actualUrl = url.hostname.toString() + url.pathname.toString();
    assert.equal(actualUrl, "staging.bsa-hedonist.online/search");

    browser.reloadSession();
  });

//not working
  xit('should create new place on Hedonist', () => {
    browser.url('https://staging.bsa-hedonist.online/places/add');
    
    const nameField = $('input.input.is-medium');
    const cityField = $('div.place-location input[placeholder="Location"]');
    const zipField = $('input.input[placeholder="09678"');
    const adressField = $('input.input[placeholder="Khreschatyk St., 14"]');
    const phoneField = $('input.input[type=tel');
    const websiteField = $('input.input[placeholder="http://the-best-place.com/"');
    const descriptionField = $('textarea');
    const nextButton = $('div.tab-item:not([style="display: none;"]) span.is-success');

    const uploadWindow = $('input[type=file]');
    const buttonSelector = 'div.tab-item:not([style="display: none;"]) span.is-success';
    let button = $(buttonSelector);
    
    nameField.setValue('PandaPizzza');
    cityField.clearValue();
    cityField.setValue('Current Position');
    zipField.setValue('87965');
    adressField.setValue('Sich., 55');
    phoneField.setValue('+380951478826');
    websiteField.setValue('www.pandasecurity.com');
    descriptionField.setValue('PandaPizzza is the best!');
    nextButton.click();
    
    uploadWindow.setValue('C:/Users/Tetiana/Pictures/panda.jpg');
    button = $(buttonSelector); 
    button.click();

    button = $(buttonSelector);
    button.click();

    browser.pause(3000);
  });

 xit('should create new list on Hedonist', () => {
    browser.url('https://staging.bsa-hedonist.online/login');
    userLogin('12QWE@yandex.ru', '123456789');
    
    waitForMap();

    browser.url('https://staging.bsa-hedonist.online/my-lists/add');
  
    const nameField = $('input[id="list-name"');
    const saveButton = $('button.button.is-success');
    const notification = $('.notices.is-top');

    nameField.setValue(newListTitle);
    saveButton.click();
    notification.waitForDisplayed();

    assert.equal(notification.getText(), "The list was saved!");

    const createdLists = $('h3[class="title has-text-primary"]');
    assert.equal(createdLists.getText(), newListTitle); 
    
    browser.reloadSession();
  });
  
  xit('should delete list on Hedonist', () => {
    browser.url('https://staging.bsa-hedonist.online/login');
    userLogin('12QWE@yandex.ru', '123456789');

    waitForMap();
    
    browser.url('https://staging.bsa-hedonist.online/my-lists');
    
    const placeItem = $('.place-item');
    placeItem.waitForDisplayed(10000);
    
    const firstItemTitle = $('h3[class="title has-text-primary"]').getText();

    browser.execute(() => {
      document.querySelectorAll('.place-item__actions .button.is-danger')[0].click();
    });

    browser.execute(() => {
      document.querySelectorAll('.animation-content.modal-content .button.is-danger')[0].click();
    });
    
    const notification = $('div.notices.is-top');
    notification.waitForDisplayed();

    assert.equal(notification.getText(), "The list was removed");

    const newFirstItemTitle = $('h3[class="title has-text-primary"]').getText();
    assert.notEqual(firstItemTitle, newFirstItemTitle);

    browser.reloadSession();
  });
});
