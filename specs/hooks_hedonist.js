describe('hooks', function() {

    before(function() {
      // runs before all tests in this block
      console.log("block ---before");
    });
  
    after(function() {
      // runs after all tests in this block
      console.log("block ---after");
    });
  
    beforeEach(function() {
      // runs before each test in this block
      console.log("block ---beforeEach");
    });
  
    afterEach(function() {
      // runs after each test in this block
      console.log("block ---afterEach");
    });
  
    // test cases

    it('should do first thing', () => {
        console.log("Test-1");
    });

    it('should do second thing', () => {
        console.log("Test-2");
    });

    it('should do third thing', () => {
        console.log("Test-3");
    });

    it('should do fourth thing', () => {
        console.log("Test-4");
    });

  });