const HeaderPage = require('./header_po');
const page = new HeaderPage();

class HeaderActions {
  moveToRegistration() {
    page.SignUp.moveTo();
    page.SignUp.click();
  }
}

module.exports = HeaderActions;
