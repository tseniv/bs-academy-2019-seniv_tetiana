class CreateListPage {

  get dropdown() { return $('div.profile'); }
  get lists() { return $('a[href="/my-lists"]'); }
  get addListButton() { return $('a[href="/my-lists/add"]'); }
  get listNameInput() { return $('input[id="list-name"'); }
  get saveButton() { return $('button.button.is-success'); }
  get firstListTitle() { return $('h3[class="title has-text-primary"]'); }
  get updateButton() {return $('div.form-actions .button.is-info'); }
  
}

module.exports = CreateListPage;
