const credentials = require('../hedonistTestData.json');
const Helpers = require('./helpers/helpers');
const Assert = require('./helpers/validators');
const Wait = require('./helpers/waiters');
const CreateListPage = require('./create list/createList_pa');
const LogInPage = require('./logIn/logIn_po');
const SignUpPage = require('./signUp/signUp_po')

const login = new LogInPage();
const create = new CreateListPage();
const signup = new SignUpPage();

describe('test', () => {

    afterEach(() => {
        browser.reloadSession();
      });
    
    it('should not login with wrong email', () => {
        Helpers.loginWithCustomUser(credentials.password, credentials.email);
        Assert.wrongValueIndicationOnField(login.emailInput);
    });

    it('should not create list with empty name field', () => {
        Helpers.loginWithDefaultUser(credentials);
        Wait.forSpinner();
        create.navigateToLists();
        Wait.forSpinner();
        create.addList();
        create.saveChanges();
        Assert.errorNotificationTextIs('List name is required!');

    });
    
    it('should not register with created user', () => {
        Helpers.signupWithCreatedUser();
        Assert.wrongValueIndicationOnField(signup.emailInput);

    });
    
    it('should edit list', () => {
        Helpers.loginWithDefaultUser(credentials);
        Wait.forSpinner();
        create.navigateToLists();
        Wait.forSpinner();
        Helpers.clickItemInList();
        Wait.forSpinner();
        //create.clearField();
        create.enterListName('666');
        create.updateButton();
        Wait.forSpinner();
    });


});