const LoginActions = require('../logIn/logIn_pa');
const page = new LoginActions();
const credentials = require('../../hedonistTestData.json');
const Wait = require('./waiters')

const SignUpActions = require('../signUp/signUp_pa');
const registration = new SignUpActions();

class HelpClass {
    loginWithDefaultUser() {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(credentials.email);
        page.enterPassword(credentials.password);
        page.loginButton();

    }

    loginWithCustomUser(email, password) {
        browser.maximizeWindow();
        browser.url(credentials.appUrl);

        page.enterEmail(email);
        page.enterPassword(password);
        page.loginButton();
    }

    signupWithCreatedUser() {
        browser.maximizeWindow();
        browser.url(credentials.signupUrl);

        registration.enterFirstName(credentials.usedFirstNameInput);
        registration.enterLastName(credentials.usedLastNameInput);
        registration.enterEmail(credentials.usedEmailInput);
        registration.enterNewPassword(credentials.usedNewPassField);
        registration.createUser();

    }

    clickItemInList(name) {
        const place = $$('.place-item .place-item__actions .button.is-info');
        if (place.length === 0) {
            throw new Error("Element not found");
        }
        Wait.forLoader();
        //place[0].scrollIntoView();
        place[0].click();
    }

}

module.exports = new HelpClass();
