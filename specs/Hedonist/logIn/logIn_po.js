class LogInPage {

  get emailInput() { return $('input[name=email]'); }
  get newPasswordInput() { return $('input[type=password]'); }
  get loginButton() { return $('button.button.is-primary'); }

}

module.exports = LogInPage;
