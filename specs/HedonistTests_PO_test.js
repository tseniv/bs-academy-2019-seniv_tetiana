const assert = require('assert');
const { URL } = require('url');

const credentials = require('./hedonistTestData.json');
const HeaderActions = require('./Hedonist/header/header_pa');
const SignUpActions = require('./Hedonist/signUp/signUp_pa');
const LogInActions = require('./Hedonist/logIn/logIn_pa');
const CreateListActions = require('./Hedonist/create list/createList_pa');
const DeleteListActions = require('./Hedonist/delete list/deleteList_pa');

const headerSteps = new HeaderActions();
const signUpSteps = new SignUpActions();
const logInSteps = new LogInActions();
const createListSteps = new CreateListActions();
const deleteListSteps = new DeleteListActions();

const random = Math.floor(Math.random() * 1000000);
const testMail = `test-mail-${random}@gmail.com`;
const newListTitle = `Baba-${random}`;

function userLogin(email, password) {
  const emailField = $('input[name="email"]');
  const passField = $('input[type="password"]');
  const loginButton = $('button.is-primary');
  
  emailField.clearValue();
  emailField.setValue(email);
  passField.clearValue();
  passField.setValue(password);
  loginButton.click();
}

function waitForSpinner() {
  const spinner = $('div#preloader');
  spinner.waitForDisplayed(10000);
  spinner.waitForDisplayed(10000, true);
}

function waitForMap() {
  const map = $('div#map');
  map.waitForDisplayed(10000);
}

function waitForNotification() {
  const notification = $('.notices.is-top .toast div');
  notification.waitForDisplayed(10000);
}

function getActualUrl() {
  return browser.getUrl();
}

function getNotificationText() {
  return $('.notices.is-top .toast div').getText();
}

describe('Hedonist', () => {

  beforeEach(() => {
    browser.maximizeWindow();
    browser.url('https://staging.bsa-hedonist.online');
  });

  afterEach(() => {
    browser.reloadSession();
  });

  it('should register on Hedonist', () => {
    headerSteps.moveToRegistration();

    signUpSteps.enterFirstName('QWE');
    signUpSteps.enterLastName('RTY');
    signUpSteps.enterEmail(testMail);
    signUpSteps.enterNewPassword('123456789');
    signUpSteps.createUser();
    
    waitForNotification();
   
    assert.strictEqual(getNotificationText(), 'You have successfully registered! Now you need to login'); 
    assert.equal(getActualUrl(), 'https://staging.bsa-hedonist.online/login');

    // register with created user
    browser.refresh();
    headerSteps.moveToRegistration();

    signUpSteps.enterFirstName(credentials.usedFirstNameInput);
    signUpSteps.enterLastName(credentials.usedLastNameInput);
    signUpSteps.enterEmail(credentials.usedEmailInput);
    signUpSteps.enterNewPassword(credentials.usedNewPassInput);
    signUpSteps.createUser();

    assert.strictEqual(signUpSteps.getInvalidEmailErrorMessage(), 'This email is already registered.');
    assert.equal(getActualUrl(), 'https://staging.bsa-hedonist.online/signup');
  });

  it('should login to Hedonist', () => {
    logInSteps.enterEmail(credentials.email);
    logInSteps.enterPassword(credentials.password);
    logInSteps.loginButton();
    
    waitForNotification();
    assert.strictEqual(getNotificationText(), 'Welcome!');

    waitForMap();

    const url =  new URL(browser.getUrl());
    const actualUrl = `${url.hostname}${url.pathname}`;
    assert.equal(actualUrl, 'staging.bsa-hedonist.online/search');
  });

  it('should create new list on Hedonist', () => {
    userLogin('12QWE@yandex.ru', '123456789');
    waitForMap();

    createListSteps.moveToMenu();
    createListSteps.navigateToLists();

    waitForSpinner();
    createListSteps.addList();
    createListSteps.enterListName(newListTitle);
    createListSteps.saveChanges();
    
    waitForNotification();
    assert.equal(getNotificationText(), 'The list was saved!');
    
    createListSteps.navigateToLists();
    assert.equal(createListSteps.getFirstListTitle(), newListTitle);
  });
  
  it('should delete list on Hedonist', () => {
    userLogin('12QWE@yandex.ru', '123456789');
    waitForMap();
    
    deleteListSteps.navigateToLists();
    waitForSpinner();
    
    // title of the list that will be deleted
    const firstItemTitle = deleteListSteps.getFirstListTitle();

    deleteListSteps.deleteFirstList();
    deleteListSteps.confirmListDelete();
    
    waitForNotification();
    assert.equal(getNotificationText(), 'The list was removed');

    // check if first list was removed from the DOM by title
    const newFirstItemTitle = deleteListSteps.getFirstListTitle();
    assert.notEqual(firstItemTitle, newFirstItemTitle);
  });
});
